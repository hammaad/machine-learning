#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 15 20:14:49 2017

@author: apple
"""

# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""


import math


def add(v1, v2):
    result = []
    
    for i in range(len(v1)):
        result.append( v1[i] + v2[i])
        
    return result


def sub(v1, v2):
    result = []
    
    for i in range(len(v1)):
        result.append( v1[i] - v2[i])
        
    return result


def dot(v1, v2):
    result = []
    
    for i in range(len(v1)):
        for j in range(len(v2)):
            result.append( v1[i] * v2[j])
        
    return result




def mag(value):

#    answer = ((value[0]**2) + (value[1]**2))**0.5    

    answer = 0
    
    for i in range(len(value)):
        answer += (value[i]**2)
    
    answer = answer**0.5
 

    return answer
    

    
a = [8.218, -9.341]
b = [-1.129, 2.111]
print("Add: ", add(a,b))


c = [7.119,8.215]  
d = [-8.223,0.878]
print("Subtract: ", sub(c,d))


e = [7.41]
f = [1.671,-1.012,-0.318]
print("Dot: ", dot(e,f))

mag1 = [-0.221, 7.437] 
mag2 = [8.813, -1.331, -6.247] 

print("magnitude-1: ", mag(mag1))
print("magnitude-2: ", mag(mag2))

dir1 = [5.581, -2.136]
dir2 = [1.996, 3.108, -4.554]

dir_mag1 = [1/mag(dir1)]
dir_mag2 = [1/mag(dir2)]

print("direction-1:", dot(dir_mag1, dir1 ) )
print("direction-2:", dot(dir_mag2, dir2 ) )

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 20 20:06:11 2017

@author: apple
"""

import numpy as np
import math 
 
def card_to_polar(x,y) :
    
    #we can represent p(x,y) into polar form like f(r,theta)
    polar = []

    radius = cal_radius(x,y)
    theta = find_theta(x,y)
    
    polar.append(radius)
    polar.append(theta)
    return polar
    

def find_x(r,theta):
    
    # x = rCos0  (0 = theta)
    x = r * math.cos(theta)
    return x

    
def find_y(r,theta):
    
    # y = rSin0  (0 = theta)
    y = r * math.sin(theta)
    return y

        
def cal_radius(x,y):
    
    #using pathagorium theorem r^2 = x^2 + y^2     
    radius = (x**2 + y **2) ** 0.5
    return radius
    
    
def find_theta(x, y):
    
    rad = math.atan2(y, x)
    return math.degrees(rad)
    

def check_par_orth(v1,v2):
    x1 = np.array(v1)
    y1 = np.array(v2)

    ans = np.dot(x1,y1)
    
    if(ans == 0):
        return "perpendicular"
    else :
        return "parallel"
        
    
    
    


def mag(value):

#    answer = ((value[0]**2) + (value[1]**2))**0.5    

    answer = 0    
    for i in range(len(value)):
        answer += (value[i]**2)
    
    answer = answer**0.5
 

    return answer
    
def scalar_products(v1, v2):
    
    #https://www.wikihow.com/Find-the-Angle-Between-Two-Vectors
    answer = 0
    
    for i in range(len(v1)):
        
            answer += ( v1[i] * v2[i] )
    
    return answer


def find_angle(v1, v2, sp):
    
    #https://www.wikihow.com/Find-the-Angle-Between-Two-Vectors
    
    v1_mag = 0
    v2_mag = 0
    theta = 0
    angle = 0
    
    v1_mag =  mag(v1)
    v2_mag =  mag(v2)
    #scalar_product_ans = scalar_products(v1,v2)
    
    theta = (sp / (v1_mag * v2_mag))
    
    angle = math.acos(theta)
    
    return round(math.degrees(angle),2)
    
   
    
    


x = 3
y = 2

r = cal_radius(x,y)
theta = find_theta(x,y)
x1 = find_x(r,theta)
y1 = find_y(r,theta)
polar = card_to_polar(x1,y1)

print("Radius: " , r)
print("Theta: ", theta )
print("X Value: ", x1)
print("Y Value: ", y1)
print("Polar Form: ", polar)


a = [2,2]
b = [0,3]

scalar_product_ans = scalar_products(a,b)
angel = find_angle(a,b,scalar_product_ans)

print("scalar_product of 2 vectors: ", scalar_product_ans)
print("angle between 2 vectors: ", angel)


x1 = np.array([1,2,3])
y1 = np.array([-7,8,9])

x2 = np.array([5,10])
y2 = np.array([6,-3])
print("dot product" , np.dot(x2,y2))
print("cross product" , np.cross(x1,y1))

print(check_par_orth(x1,y1))



